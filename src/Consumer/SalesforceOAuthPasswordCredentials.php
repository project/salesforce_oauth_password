<?php

namespace Drupal\salesforce_oauth_password\Consumer;

use Drupal\salesforce\Consumer\SalesforceCredentials;

/**
 * Salesforce credentials extension, for drupalisms.
 */
class SalesforceOAuthPasswordCredentials extends SalesforceCredentials {

  /**
   * The username.
   *
   * @var string
   */
  protected $username;

  /**
   * The user's password.
   *
   * @var string
   */
  protected $password;

  /**
   * The user’s security token.
   *
   * You must append the user’s security token to their password.
   * A security token is an automatically-generated key from Salesforce.
   * For example, if a user's password is mypassword, and their security token
   * is XXXXXXXXXX, then the value provided for this parameter must be
   * mypasswordXXXXXXXXXX.
   *
   * For more information on security tokens see “Reset Your Security Token”
   * in the online help.
   *
   * @var string
   */
  protected $securityToken;

  /**
   * {@inheritdoc}
   */
  public function __construct($consumerKey, $consumerSecret, $loginUrl, $username, $password, $securityToken) {
    parent::__construct($consumerKey, $consumerSecret, NULL);
    $this->consumerKey = $consumerKey;
    $this->loginUrl = $loginUrl;
    $this->username = $username;
    $this->password = $password;
    $this->securityToken = $securityToken;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(array $configuration) {
    return new static(
      $configuration['consumer_key'],
      $configuration['consumer_secret'],
      $configuration['login_url'],
      $configuration['username'],
      $configuration['password'],
      $configuration['security_token']
    );
  }

  /**
   * The username.
   */
  public function getUsername(): string {
    return $this->username;
  }

  /**
   * The user's password.
   */
  public function getPassword(): string {
    return $this->password;
  }

  /**
   * The user’s security token.
   *
   * @see self::securityToken
   */
  public function getSecurityToken(): string {
    return $this->securityToken;
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    return !empty($this->consumerSecret)
      && !empty($this->consumerId)
      && !empty($this->username)
      && !empty($this->password);
  }

}
