<?php

namespace Drupal\salesforce_oauth_password\Plugin\SalesforceAuthProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\salesforce\SalesforceAuthProviderPluginBase;
use OAuth\Common\Token\TokenInterface;

/**
 * Salesforce OAuth user-agent flow auth provider plugin.
 *
 * @Plugin(
 *   id = "oauth_password",
 *   label = @Translation("Salesforce OAuth Password"),
 *   credentials_class = "\Drupal\salesforce_oauth_password\Consumer\SalesforceOAuthPasswordCredentials"
 * )
 *
 * @see https://developer.salesforce.com/docs/atlas.en-us.202.0.api_rest.meta/api_rest/intro_understanding_username_password_oauth_flow.htm
 */
class SalesforceOAuthPasswordPlugin extends SalesforceAuthProviderPluginBase {

  /**
   * Credentials.
   *
   * @var \Drupal\salesforce_oauth_password\Consumer\SalesforceOAuthPasswordCredentials
   */
  protected $credentials;

  /**
   * {@inheritdoc}
   */
  public static function defaultConfiguration() {
    $defaults = parent::defaultConfiguration();
    return array_merge($defaults, [
      'consumer_secret' => '',
      'username' => '',
      'password' => '',
      'security_token' => '',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['consumer_key'] = [
      '#title' => $this->t('Salesforce consumer key'),
      '#type' => 'textfield',
      '#description' => $this->t('Consumer key of the Salesforce remote application you want to grant access to'),
      '#required' => TRUE,
      '#default_value' => $this->getCredentials()->getConsumerKey(),
    ];

    $form['consumer_secret'] = [
      '#title' => $this->t('Salesforce consumer secret'),
      '#type' => 'textfield',
      '#description' => $this->t('Consumer secret of the Salesforce remote application.'),
      '#required' => TRUE,
      '#default_value' => $this->getCredentials()->getConsumerSecret(),
    ];

    $form['username'] = [
      '#title' => $this->t('Username'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $this->getCredentials()->getUsername(),
    ];

    $form['password'] = [
      '#title' => $this->t('Password'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $this->getCredentials()->getPassword(),
    ];

    $form['security_token'] = [
      '#title' => $this->t('Security token'),
      '#type' => 'textfield',
      '#description' => $this->t('A security token is an automatically-generated key from Salesforce. For more information on security tokens see “Reset Your Security Token” in the online help.'),
      '#required' => TRUE,
      '#default_value' => $this->getCredentials()->getSecurityToken(),
    ];

    $form['login_url'] = [
      '#title' => $this->t('Login URL'),
      '#type' => 'textfield',
      '#default_value' => $this->getCredentials()->getLoginUrl(),
      '#description' => $this->t('Enter a login URL, either https://login.salesforce.com or https://test.salesforce.com.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if (empty($form_state->getValue('provider_settings')) && $form_state->getValue('provider_settings') == self::defaultConfiguration()) {
      $form_state->setError($form, $this->t('Please fill in JWT provider settings.'));
      return;
    }
    $this->setConfiguration($form_state->getValue('provider_settings'));
    // Force new credentials from form input, rather than storage.
    unset($this->credentials);
    try {
      // Bootstrap here by setting ID to provide a key to token storage.
      $this->id = $form_state->getValue('id');
      $this->requestAccessToken();
    }
    catch (\Exception $e) {
      $form_state->setError($form, $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function requestAccessToken($code = NULL, $state = NULL) {
    if ($state !== NULL) {
      $this->validateAuthorizationState($state);
    }

    $credentials = $this->getCredentials();
    $bodyParams = [
      'username'      => $credentials->getUsername(),
      'password'      => $credentials->getPassword() . $credentials->getSecurityToken(),
      'client_id'     => $credentials->getConsumerId(),
      'client_secret' => $credentials->getConsumerSecret(),
      'grant_type'    => 'password',
    ];

    $responseBody = $this->httpClient->retrieveResponse(
      $this->getAccessTokenEndpoint(),
      $bodyParams,
      $this->getExtraOAuthHeaders()
    );

    $token = $this->parseAccessTokenResponse($responseBody);
    $this->storage->storeAccessToken($this->service(), $token);

    return $token;
  }

  /**
   * {@inheritdoc}
   *
   * This OAuth authentication flow involves passing the user’s credentials back
   * and forth. No refresh token will be issued, so just get a regular access
   * token and refresh the identity.
   */
  public function refreshAccessToken(TokenInterface $token) {
    $token = $this->requestAccessToken();
    $this->refreshIdentity($token);
    return $token;
  }

}
