# Salesforce OAuth Password Provider

Provides password-based Salesforce OAuth authentication. 

> You can use the username-password flow to authorize a client via a connected app
that already has the user’s credentials. However, we recommend avoiding this
flow because it passes credentials back and forth. Use it only if there’s a high
degree of trust between the resource owner and the client, the client is a
first-party app, Salesforce is hosting the data, and other grant types aren’t
available. In these cases, set user permissions to minimize access and protect
stored credentials from unauthorized access.

Source: [salesforce.com](https://help.salesforce.com/s/articleView?id=sf.remoteaccess_oauth_username_password_flow.htm&type=5)


## Requirements

 This module is an extension to the [Salesforce module](https://www.drupal.org/project/salesforce).


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at _Administration > Extend_.
2. Visit _Configuration > Salesforce > Salesforce Authorization_ and add a provider of type _Salesforce OAuth Password_.


## Maintainers

Current maintainers:
- Dieter Holvoet - [DieterHolvoet](https://www.drupal.org/u/dieterholvoet)
